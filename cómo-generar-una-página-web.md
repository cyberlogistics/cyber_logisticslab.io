1.   Para cada ferretería, seleccionar un responsable para una cuenta de correo.

2.   [Hacer una cuenta de GitLab](https://gitlab.com/users/sign_in )

     ![](/home/x/shots/2021-01-13-115734.png)

3.   [Generar un repositorio llamado <cuenta.gitlab.io>](https://gitlab.com/projects/new )

4.   Guardar el proyecto en tu computadora

     ```
     git clone https://gitlab.com/cuenta/cuenta.gitlab.io
     ```

5.   Descargar la base de proyecto para clientes:

     ```
     https://gitlab.com/cyberlogistics/@@download-uri@@
     ```

6.   Descomprimir el proyecto en el directorio de usuario

     ```
     unzip [archivo-descargado]
     rm [archivo-descargado]
     ```

7.   Cambiar los datos de `config.toml`

8.   Guardar el proyecto en git:

     ```
     git add . && git commit -m 'Agregar datos de la empresa.'
     ```

8.   Subir el proyecto a GitLab.

     ```
     git push -u origin master
     ```

9.   La página está pública.

## Registrar una cuenta de Google Maps


## Referencias

[GitHub Pages Tutorial](https://guides.github.com/features/pages/ )
