/*jslint browser: true*/
/*global $, jQuery, alert*/
"use strict";

function show_message(msg) {
    $('#response').html('</br></br><h4>' + msg + '</h4>');
}

$(document).on('submit', '#contact-form', function( e ) {

    e.preventDefault();

    $.ajax( {
        url: $(this).attr('action'),
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false
    }).done(function( data ) {
        show_message(data.message)
    });

});
