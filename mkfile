help:QV: ## show this help
	awk 'BEGIN {FS = "\t|:.*?## "};
	/[ \t]##[ \t]/ {printf "\033[36m%-20s\033[0m %s\n", $1, $NF}' \
		mkfile \
	| sort

env:QV:
	pip3 install pre-commit
	pre-commit install

config-filter:VQ:
	git config --local filter.test-cms.clean $(pwd)/tools/test-cms-clean
	git config --local filter.test-cms.smudge $(pwd)/tools/test-cms-smudge
	git config --local filter.test-site.clean $(pwd)/tools/test-site-clean
	git config --local filter.test-site.smudge $(pwd)/tools/test-site-smudge
	git config --local filter.test-warranty.clean $(pwd)/tools/test-warranty-clean
	git config --local filter.test-warranty.smudge $(pwd)/tools/test-warranty-smudge

test:V:	clean	## Build the site and run it using containers
	tools/build
	tools/serve

install:VQ:
	brew install hugo

serve:VQ: ## launch an instance of the server
	cd website
	hugo server --disableFastRender

deploy:VQ: ## deploy the website
	git push origin master

start:VQ:	env	config-filter	install	## initial configuration

website/static/downloads/cyber-promo.zip:	../cyber_logistics.gitlab.io-clientes/
	REPO=$(pwd)
	cd ${prereq}
	git submodule update
	rm -rf website/themes/meghna/exampleSite website/themes/meghna/.git*
	rm .git
	zip -r ${REPO}/${target} .

../cyber_logistics.gitlab.io-clientes/:
	git worktree add ../cyber_logistics.gitlab.io-clientes/ clientes


clean:VQ:
	docker stop nginx-cyber || true
	rm -rf public ../cyber_logistics.gitlab.io-clientes/ website/static/downloads/cyber-promo.zip
	git worktree prune
