Para Cyberlogistics
que quieren hacer una campaña publicitaria para sus clientes
este manual de procedimientos
enseña a generar páginas web estáticas
y a diferencia de levantar un sitio web de wordpress
nuestro producto no requiere soporte continuo.

## Documentación Accesoria

- [Cotización que contiene los entregables y expectativas del cliente](./docs/CotizaciónWebPromocionalesCyber.pdf).

- [Información de los textos](https://pad.lqdn.fr/p/cyberwebs2021 ).

- [Pendientes para entregar](https://pad.lqdn.fr/p/cyberlogisticsPendientes ).

- [Cuentas de los proyectos](https://ethercalc.net/5hqfs1g8fk7d ).
